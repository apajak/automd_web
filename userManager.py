import os
import uuid

class User:
    def __init__(self, id, name, password, email, role,profile_image,api_key,openai_api_key,mail_token,workdir):
        self.id = id
        self.name = name
        self.password = password
        self.email = email
        self.role = role
        self.profile_image = profile_image
        self.api_key = api_key
        self.openai_api_key = openai_api_key
        self.mail_token = mail_token
        self.workdir = workdir

    def json(self):
        return {"id": self.id,
                "name": self.name,
                "password": self.password,
                "email": self.email,
                "role": self.role,
                "profile_image": self.profile_image,
                "api_key": self.api_key,
                "openai_api_key": self.openai_api_key,
                "mail_token": self.mail_token,
                "workdir": self.workdir
                }
    def __str__(self):
        return (f"User(id={self.id}, name={self.name}, password={self.password}, email={self.email}, role={self.role},"
                f" profile_image={self.profile_image}, api_key={self.api_key}, openai_api_key={self.openai_api_key},"
                f" mail_token={self.mail_token}), workdir={self.workdir})")



