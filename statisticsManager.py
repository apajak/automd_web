import random
from datetime import datetime, timedelta

class Statistics:
    def __init__(self, markdowns_count, histories_count, users_count,
                 markdowns_generated, markdowns_downloaded,
                 histories_downloaded, histories_uploaded, app_downloads,id=None,created_at=None):
        self.id = id
        self.markdowns_count = markdowns_count
        self.histories_count = histories_count
        self.users_count = users_count
        self.markdowns_generated = markdowns_generated
        self.markdowns_downloaded = markdowns_downloaded
        self.histories_downloaded = histories_downloaded
        self.histories_uploaded = histories_uploaded
        self.app_downloads = app_downloads
        self.created_at = created_at
    @classmethod
    def from_db_row(cls, row):
        year = row[0]
        month = row[1]
        date = datetime(year, month, 1)
        return cls(
            markdowns_count=int(row[2]),  # Convert to integer to remove decimals
            histories_count=int(row[3]),
            users_count=int(row[4]),
            markdowns_generated=int(row[5]),
            markdowns_downloaded=int(row[6]),
            histories_downloaded=int(row[7]),
            histories_uploaded=int(row[8]),
            app_downloads=int(row[9]),
            created_at= date.strftime("%Y-%m")
        )

    @classmethod
    def from_db_row_weekly(cls, row):
        year = row[0]
        week = row[1]
        date = datetime.strptime(f"{year}-{week}-1", "%Y-%U-%w")
        return cls(
            markdowns_count=int(row[2]),
            histories_count=int(row[3]),
            users_count=int(row[4]),
            markdowns_generated=int(row[5]),
            markdowns_downloaded=int(row[6]),
            histories_downloaded=int(row[7]),
            histories_uploaded=int(row[8]),
            app_downloads=int(row[9]),
            created_at=date.strftime("%Y-%U")
        )

    def json(self):
        return {"id": self.id,
                "markdowns_count": self.markdowns_count,
                "histories_count": self.histories_count,
                "users_count": self.users_count,
                "markdowns_generated": self.markdowns_generated,
                "markdowns_downloaded": self.markdowns_downloaded,
                "histories_downloaded": self.histories_downloaded,
                "histories_uploaded": self.histories_uploaded,
                "app_downloads": self.app_downloads,
                "created_at": self.created_at
                }

    def __str__(self):
        return (f"id={self.id},"
                f"markdowns_count={self.markdowns_count}, "
                f"histories_count={self.histories_count}, "
                f"users_count={self.users_count}, "
                f"markdowns_generated={self.markdowns_generated}, "
                f"markdowns_downloaded={self.markdowns_downloaded}, "
                f"histories_downloaded={self.histories_downloaded}, "
                f"histories_uploaded={self.histories_uploaded}, app_downloads={self.app_downloads}) "
                f"created_at={self.created_at})")

def generate_test_statistics():
    # Generate random statistics data within reasonable ranges
    markdowns_count = random.randint(100, 1000)
    histories_count = random.randint(50, 500)
    users_count = random.randint(10, 100)
    markdowns_generated = random.randint(1000, 10000)
    markdowns_downloaded = random.randint(500, 5000)
    histories_downloaded = random.randint(250, 2500)
    histories_uploaded = random.randint(200, 2000)
    app_downloads = random.randint(1000, 10000)

    # Generate a random datetime for created_at (within the last 30 days)
    end_date = datetime.now()
    start_date = end_date - timedelta(days=90)
    created_at = start_date + timedelta(seconds=random.randint(0, int((end_date - start_date).total_seconds())))
    created_at = created_at.strftime("%Y-%m-%d %H:%M:%S")

    # Create a Statistics object with the generated data
    statistics = Statistics(markdowns_count, histories_count, users_count,
                            markdowns_generated, markdowns_downloaded,
                            histories_downloaded, histories_uploaded, app_downloads,
                            created_at=created_at)

    return statistics