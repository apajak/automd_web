class History:
    def __init__(self,id,user_id,content,created_at,title):
        self.id = id
        self.user_id = user_id
        self.content = content
        self.created_at = created_at
        self.title = title

    def json(self):
        return {"id": self.id,
                "user_id": self.user_id,
                "content": self.content,
                "created_at": self.created_at,
                "title": self.title
                }
    def __str__(self):
        return f"History(id={self.id}, user_id={self.user_id}, content={self.content}, created_at={self.created_at})," \
               f" title={self.title}"