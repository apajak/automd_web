import mysql.connector
from mysql.connector import errorcode
import bcrypt
from userManager import User
from projectManager import Project
from historyManager import History
from statisticsManager import Statistics
import secrets
from tokenManager import DownloadToken
import datetime



class MySQLManager:
    def __init__(self, host, user, password, database):
        self.host = host
        self.user = user
        self.password = password
        self.database = database
        self.cnx = None
        self.cursor = None

    def connect(self):
        try:
            self.cnx = mysql.connector.connect(host=self.host,
                                               user=self.user,
                                               password=self.password,
                                               database=self.database,
                                               autocommit=True)
            self.cursor = self.cnx.cursor()
        except mysql.connector.Error as err:
            if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
                print("Something is wrong with your user name or password")
            elif err.errno == errorcode.ER_BAD_DB_ERROR:
                print("Database does not exist")
            else:
                print(err)


    def close(self):
        if self.cursor:
            self.cursor.close()
        if self.cnx:
            self.cnx.close()

    def execute(self, query, args=None):
        if args is None:
            self.cursor.execute(query)
        else:
            self.cursor.execute(query, args)

    def fetchall(self):
        return self.cursor.fetchall()

    def fetchone(self):
        return self.cursor.fetchone()

    def get_last_row_id(self):
        return self.cursor.lastrowid

    def get_row_count(self):
        return len(self.fetchall())

    # User
    def tuple_to_user(self, tuple):
        return User(tuple[0], tuple[1], tuple[2], tuple[3], tuple[4], tuple[5], tuple[6], tuple[7], tuple[8],tuple[9])


    def user_to_tuple(self, user):
        tuple_user = (user.id, user.name, user.password, user.email, user.role, user.profile_image, user.api_key,
                 user.openai_api_key, user.mail_token,user.workdir)
        return tuple_user


    def get_user_by_id(self, id):
        self.connect()
        self.execute("SELECT * FROM users WHERE id=%s", (id,))
        user = self.tuple_to_user(self.fetchone())
        self.close()
        return user


    def get_user_by_name(self, name):
        self.connect()
        self.execute("SELECT * FROM users WHERE name=%s", (name,))
        user = self.tuple_to_user(self.fetchone())
        self.close()
        return user


    def get_user_by_email(self, email):
        self.connect()
        self.execute("SELECT * FROM users WHERE email=%s", (email,))
        user = self.tuple_to_user(self.fetchone())
        self.close()
        return user


    def get_user_by_api_key(self, api_key):
        self.connect()
        self.execute("SELECT * FROM users WHERE api_key=%s", (api_key,))
        user = self.tuple_to_user(self.fetchone())
        self.close()
        return user


    def get_user_mail_token(self, user):
        self.connect()
        self.execute("SELECT mail_token FROM users WHERE id=%s", (user.id,))
        self.close()
        return self.fetchone()[0]

    def get_user_by_mail_token(self, mail_token):
        self.connect()
        self.execute("SELECT * FROM users WHERE mail_token=%s", (mail_token,))
        user = self.tuple_to_user(self.fetchone())
        self.close()
        return user
    def get_user_api_key(self, user):
        self.connect()
        self.execute("SELECT api_key FROM users WHERE id=%s", (user.id,))
        self.close()
        return self.fetchone()[0]

    def user_exists(self, username, mail):
        self.connect()
        try:
            self.execute("SELECT * FROM users WHERE name=%s OR email=%s", (username, mail))
            count = self.get_row_count()
            self.close()
            return count > 0
        except:
            self.close()
            return False

    def email_exists(self, email):
        self.connect()
        try:
            self.execute("SELECT * FROM users WHERE email=%s", (email,))
            count = self.get_row_count()
            self.close()
            return count > 0
        except:
            self.close()
            return False
    def register(self,username,password,email,image,openai_api_key,workdir):
        # check if user exists
        if self.email_exists(email):
            return False
        # hash password
        hashed_password = bcrypt.hashpw(password.encode('utf-8'), bcrypt.gensalt())
        # set api key
        api_key = secrets.token_urlsafe(16)
        # insert user
        try:
            self.connect()
            query = "INSERT INTO users (name,password,email,profile_image,api_key,openai_api_key,workdir) VALUES (%s,%s,%s,%s,%s,%s,%s)"
            self.execute(query, (username, hashed_password, email, image, api_key, openai_api_key,workdir))
            self.close()
            return True
        except:
            self.close()
            return False

    def login(self, username, password):
        self.connect()
        query = "SELECT * FROM users WHERE name = %s OR email = %s"
        self.execute(query, (username, username))
        user = self.fetchone()
        if user is not None:
            password_hash = user[2]
            if bcrypt.checkpw(password.encode('utf-8'), password_hash.encode('utf-8')):
                return self.tuple_to_user(user)
        return None
    def add_workdir(self,user):
        self.connect()
        try:
            self.execute("UPDATE users SET workdir=%s WHERE id=%s", (user.workdir, user.id))
            self.close()
            return True
        except Exception as e:
            return False

    def update_user(self,user):
        self.connect()
        # update user
        try:
            query = "UPDATE users SET name=%s,email=%s,role=%s,profile_image=%s,api_key=%s," \
                    "openai_api_key=%s,mail_token=%s WHERE id=%s"
            args = (user.name, user.email, user.role, user.profile_image, user.api_key,
                    user.openai_api_key, user.mail_token, user.id)
            self.execute(query, args)
            self.close()
            return True
        except Exception as e:
            return False

    def remove_email_token(self,user):
        self.connect()
        try:
            self.execute("UPDATE users SET mail_token=%s WHERE id=%s", (None, user.id))
            self.close()
            return True
        except Exception as e:
            return False

    def update_user_password(self, password, id):
        query = "UPDATE users SET password = %s WHERE id = %s"
        password_hash = bcrypt.hashpw(password.encode('utf-8'), bcrypt.gensalt())
        try:
            self.connect()
            self.execute(query, (password_hash, id))
            self.close()
        except Exception as e:
            raise e

    def update_user_mail_token(self, user_id,mail_token):
        self.connect()
        try:
            self.execute("UPDATE users SET mail_token=%s WHERE id=%s", (mail_token, user_id))
            self.close()
            return True
        except Exception as e:
            return False

    def reset_password(self,user):
        self.connect()
        # reset password
        try:
            self.execute("UPDATE users SET password=%s WHERE id=%s", (user.password, user.id))
            self.close()
            return True
        except Exception as e:
            return False

    def update_user_api_key(self,user):
        self.connect()
        try:
            self.execute("UPDATE users SET api_key=%s WHERE id=%s", (user.api_key, user.id))
            self.close()
            return True
        except:
            return False


    def delete_user(self,user_id):
        self.connect()
        # delete user
        try:
            self.execute("DELETE FROM users WHERE id=%s", (user_id,))
            self.close()
            return True
        except:
            return False

    def get_all_users(self):
        self.connect()
        # get all users
        self.execute("SELECT * FROM users")
        users = [self.tuple_to_user(user) for user in self.fetchall()]
        self.close()
        return users

    def get_user_by_workdir(self,workdir):
        self.connect()
        self.execute("SELECT * FROM users WHERE workdir=%s", (workdir,))
        user = self.tuple_to_user(self.fetchone())
        self.close()
        return user

    # Project
    def tuple_to_project(self, tuple):
        return Project(tuple[0], tuple[1], tuple[2], tuple[3], tuple[4], tuple[5], tuple[6])

    def project_to_tuple(self, project):
        tuple_project = (project.id, project.owner_user_id, project.title, project.content, project.media,
                         project.history_id, project.created_at)
        return tuple_project
    def insert_project(self, project):
        self.connect()
        try:
            self.execute("INSERT INTO project (owner_user_id, title, content, media, history_id) VALUES (%s, %s, %s, %s, %s)", (project.owner_user_id, project.title, project.content, project.media, project.history_id))
            # return id of last inserted project
            self.execute("SELECT LAST_INSERT_ID()")
            last_id = self.fetchone()[0]
            self.close()
            return last_id
        except Exception as e:
            return False
    def update_project(self, project):
        self.connect()
        try:
            self.execute("UPDATE project SET title=%s, content=%s, media=%s, history_id=%s WHERE id=%s", (project.title, project.content, project.media, project.history_id, project.id))
            self.close()
            return True
        except Exception as e:
            return False

    def delete_project(self, id):
        self.connect()
        try:
            self.execute("DELETE FROM project WHERE id=%s", (id,))
            self.close()
            return True
        except Exception as e:
            return False

    def get_all_projects(self):
        self.connect()
        self.execute("SELECT * FROM project")
        projects = [self.tuple_to_project(project) for project in self.fetchall()]
        self.close()
        return projects

    def get_project_by_id(self, id):
        self.connect()
        self.execute("SELECT * FROM project WHERE id=%s", (id,))
        project = self.fetchone()
        self.close()
        return self.tuple_to_project(project)
    def get_user_project_by_title(self,user_id,title):
        self.connect()
        self.execute("SELECT * FROM project WHERE title=%s AND owner_user_id=%s", (title,user_id))
        project = self.fetchone()
        self.close()
        return self.tuple_to_project(project)

    def get_projects_by_user_id(self, user_id):
        self.connect()
        self.execute("SELECT * FROM project WHERE owner_user_id=%s", (user_id,))
        projects = [self.tuple_to_project(project) for project in self.fetchall()]
        self.close()
        return projects



    def get_projects_by_history_id(self, history_id):
        self.connect()
        self.execute("SELECT * FROM project WHERE history_id=%s", (history_id,))
        projects = [self.tuple_to_project(project) for project in self.fetchall()]
        self.close()
        return projects

    def update_project_media(self, project_id,media):
        self.connect()
        try:
            self.execute("UPDATE project SET media=%s WHERE id=%s", (media, project_id))
            self.close()
            return True
        except Exception as e:
            return False

            # history
    def tuple_to_history(self, tuple):

        return History(tuple[0], tuple[1], tuple[2], tuple[3], tuple[4])

    def history_to_tuple(self, history):
        tuple_history = (history.id, history.user_id, history.content, history.created_at, history.title)
        return tuple_history

    def insert_history(self, history):
        self.connect()
        try:
            self.execute("INSERT INTO history (user_id, content, title) VALUES (%s, %s, %s)", (history.user_id,
                                                                                               history.content,
                                                                                               history.title))
            # return id of last inserted history
            self.execute("SELECT LAST_INSERT_ID()")
            last_id = self.fetchone()[0]
            self.close()
            return last_id
        except Exception as e:
            return False
    def get_history_by_id(self, id):
        self.connect()
        self.execute("SELECT * FROM history WHERE id=%s", (id,))
        history = self.fetchone()
        self.close()
        return self.tuple_to_history(history)

    def get_all_history(self):
        self.connect()
        self.execute("SELECT * FROM history")
        history = [self.tuple_to_history(history) for history in self.fetchall()]
        self.close()
        return history

    def get_history_by_user_id(self, user_id):
        self.connect()
        self.execute("SELECT * FROM history WHERE user_id=%s", (user_id,))
        history = [self.tuple_to_history(history) for history in self.fetchall()]
        self.close()
        return history

    def update_history(self, history):
        self.connect()
        try:
            self.execute("UPDATE history SET content=%s, title=%s WHERE id=%s", (history.content,
                                                                                 history.title, history.id))
            self.close()
            return True
        except Exception as e:
            return False

    def delete_history(self, history_id):
        self.connect()
        try:
            self.execute("DELETE FROM history WHERE id=%s", (history_id,))
            self.close()
            return True
        except Exception as e:
            return False

    # token
    def tuple_to_token(self, tuple):
        return DownloadToken(tuple[0], tuple[1], tuple[2], tuple[3])

    def token_to_tuple(self, token):
        tuple_token = (token.id, token.user_id, token.token, token.created_at)
        return tuple_token

    def insert_token(self, user_id):
        token = DownloadToken(None,user_id, self.generate_token(),None)
        self.connect()
        try:
            # Check if an existing token exists for the user
            self.execute("SELECT id FROM downloads_token WHERE user_id = %s", (token.user_id,))
            existing_token = self.fetchone()  # Fetch the result

            if existing_token:
                # remove the existing token
                self.execute("DELETE FROM downloads_token WHERE user_id = %s", (token.user_id,))
                # insert a new record
                self.execute("INSERT INTO downloads_token (user_id, token) VALUES (%s, %s)", (token.user_id, token.token))
            else:
                # If no existing token is found, insert a new record
                self.execute("INSERT INTO downloads_token (user_id, token) VALUES (%s, %s)", (token.user_id, token.token))

            self.close()
            return token.token
        except Exception as e:
            return None

    def generate_token(self):
        return secrets.token_urlsafe(32)

    def check_token(self, token):
        """
        :param token: Check if the token exists in the database AND if it is not expired (less than 1 hour)
        :return: boolean True if the token is valid, False otherwise
        """
        self.connect()
        self.execute("SELECT created_at FROM downloads_token WHERE token=%s", (token,))
        created_at = self.fetchone()
        self.close()
        if created_at is None:
            return False
        else:
            created_at = created_at[0]
            # check if token is expired
            if (datetime.datetime.now() - created_at).total_seconds() > 3600:
                # token is expired
                return False
            else:
                # token is not expired
                return True



    def renew_token(self, user_id):
        token = DownloadToken(None, user_id, self.generate_token(), None)
        self.connect()
        try:
            self.execute("UPDATE downloads_token SET token = %s WHERE user_id = %s", (token.token, token.user_id))
            self.close()
            return token.token
        except Exception as e:
            return None


    def remove_token(self, token):
        self.connect()
        try:
            self.execute("DELETE FROM downloads_token WHERE token=%s", (token,))
            self.close()
            return True
        except Exception as e:
            return False

    def get_userid_by_token(self, token):
        self.connect()
        self.execute("SELECT user_id FROM downloads_token WHERE token=%s", (token,))
        user_id = self.fetchone()
        self.close()
        return user_id[0]

# Statistics
    def tuple_to_statistics(self, tuple):
        return Statistics(markdowns_count=tuple[1], histories_count=tuple[2], users_count=tuple[3],
                            markdowns_generated=tuple[4], markdowns_downloaded=tuple[5],
                            histories_downloaded=tuple[6], histories_uploaded=tuple[7], app_downloads=tuple[8],
                            id=tuple[0], created_at=tuple[9])

    def statistics_to_tuple(self, statistics):
        tuple_statistics = (statistics.id, statistics.markdowns_count, statistics.histories_count,
                            statistics.users_count, statistics.markdowns_generated, statistics.markdowns_downloaded,
                            statistics.histories_downloaded, statistics.histories_uploaded, statistics.app_downloads,
                            statistics.created_at)
        return tuple_statistics

    def insert_statistics(self, statistics):
        self.connect()
        try:
            self.execute("INSERT INTO statistics (markdowns_count, histories_count, users_count, "
                         "markdowns_generated, markdowns_downloaded, histories_downloaded,"
                         " histories_uploaded, app_downloads) VALUES (%s, %s, %s, %s, %s, %s, %s, %s)",
                         (statistics.markdowns_count, statistics.histories_count, statistics.users_count,
                          statistics.markdowns_generated, statistics.markdowns_downloaded,
                          statistics.histories_downloaded, statistics.histories_uploaded, statistics.app_downloads))
            self.close()
            return True
        except Exception as e:
            return False

    def test_insert_statistics(self, statistics):
        self.connect()
        try:
            self.execute("INSERT INTO statistics (markdowns_count, histories_count, users_count, "
                         "markdowns_generated, markdowns_downloaded, histories_downloaded,"
                         " histories_uploaded, app_downloads,created_at) VALUES (%s, %s, %s, %s, %s, %s, %s, %s,%s)",
                         (statistics.markdowns_count, statistics.histories_count, statistics.users_count,
                          statistics.markdowns_generated, statistics.markdowns_downloaded,
                          statistics.histories_downloaded, statistics.histories_uploaded, statistics.app_downloads,statistics.created_at))
            self.close()
            return True
        except Exception as e:
            return False
    def get_all_statistics(self):
        self.connect()
        self.execute("SELECT * FROM statistics")
        statistics = [self.tuple_to_statistics(statistics) for statistics in self.fetchall()]
        self.close()
        return statistics

    def get_statistics_by_id(self, id):
        self.connect()
        self.execute("SELECT * FROM statistics WHERE id=%s", (id,))
        statistics = self.fetchone()
        self.close()
        return self.tuple_to_statistics(statistics)

    def get_last_statistics(self):
        self.connect()
        self.execute("SELECT * FROM statistics ORDER BY id DESC LIMIT 1")
        statistics = self.tuple_to_statistics(self.fetchone())
        self.close()
        return statistics

    def row_to_monthly_statistics(self, row):
        return {
            "year": row[0],
            "month": row[1],
            "median_markdowns_count": row[2],
            "median_histories_count": row[3],
            "median_users_count": row[4],
            "median_markdowns_generated": row[5],
            "median_markdowns_downloaded": row[6],
            "median_histories_downloaded": row[7],
            "median_histories_uploaded": row[8],
            "median_app_downloads": row[9]
        }

    def get_monthly_statistics(self):
        self.connect()
        self.execute("""
            SELECT
                YEAR(created_at) AS year,
                MONTH(created_at) AS month,
                AVG(markdowns_count) AS median_markdowns_count,
                AVG(histories_count) AS median_histories_count,
                AVG(users_count) AS median_users_count,
                AVG(markdowns_generated) AS median_markdowns_generated,
                AVG(markdowns_downloaded) AS median_markdowns_downloaded,
                AVG(histories_downloaded) AS median_histories_downloaded,
                AVG(histories_uploaded) AS median_histories_uploaded,
                AVG(app_downloads) AS median_app_downloads
            FROM statistics
            GROUP BY year, month
        """)

        monthly_statistics = [Statistics.from_db_row(row) for row in self.fetchall()]
        self.close()
        return monthly_statistics

    def get_weekly_statistics(self):
        self.connect()
        self.execute("""
            SELECT
                YEAR(created_at) AS year,
                WEEK(created_at) AS week,
                AVG(markdowns_count) AS median_markdowns_count,
                AVG(histories_count) AS median_histories_count,
                AVG(users_count) AS median_users_count,
                AVG(markdowns_generated) AS median_markdowns_generated,
                AVG(markdowns_downloaded) AS median_markdowns_downloaded,
                AVG(histories_downloaded) AS median_histories_downloaded,
                AVG(histories_uploaded) AS histories_uploaded,
                AVG(app_downloads) AS median_app_downloads
            FROM statistics
            GROUP BY year, week
        """)

        weekly_statistics = [Statistics.from_db_row_weekly(row) for row in self.fetchall()]
        self.close()
        return weekly_statistics

    def get_daily_statistics(self):
        self.connect()
        self.execute("SELECT * FROM statistics WHERE DAY(created_at) = DAY(CURRENT_DATE())")
        statistics = [self.tuple_to_statistics(statistics) for statistics in self.fetchall()]
        self.close()
        return statistics

    def last_statistics_is_today(self, statistics):
        # Get today's date as a date object (without the time)
        today = datetime.date.today()

        # Extract the date part from the 'created_at' attribute of the statistics object
        statistics_date = statistics.created_at.date()

        # Compare the date parts to check if they are equal (i.e., if it's the same day)
        return today == statistics_date

    def update_statistics(self, statistics):
        self.connect()
        try:
            self.execute("UPDATE statistics SET markdowns_count=%s, histories_count=%s, users_count=%s, "
                         "markdowns_generated=%s, markdowns_downloaded=%s, histories_downloaded=%s,"
                         " histories_uploaded=%s, app_downloads=%s WHERE id=%s",
                         (statistics.markdowns_count, statistics.histories_count, statistics.users_count,
                          statistics.markdowns_generated, statistics.markdowns_downloaded,
                          statistics.histories_downloaded, statistics.histories_uploaded, statistics.app_downloads,
                          statistics.id))
            self.close()
            return True
        except Exception as e:
            print(e)
            return False


