class Project:
    def __init__(self,id,owner_user_id,title,content,media,history_id,created_at):
        self.id = id
        self.owner_user_id = owner_user_id
        self.title = title
        self.content = content
        self.media = media
        self.history_id = history_id
        self.created_at = created_at

    def json(self):
        return {"id": self.id,
                "owner_user_id": self.owner_user_id,
                "title": self.title,
                "content": self.content,
                "media": self.media
                }
    def __str__(self):
        return f"Project(id={self.id}, owner_user_id={self.owner_user_id}, title={self.title}, content={self.content}, media={self.media})"