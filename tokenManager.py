class DownloadToken:
    def __init__(self, id, user_id,token,created_at):
        self.id = id
        self.user_id = user_id
        self.token = token
        self.created_at = created_at

    def json(self):
        return {"id": self.id,
                "user_id": self.user_id,
                "token": self.token,
                "created_at": self.created_at
                }
    def __str__(self):
        return f"DownloadToken(id={self.id}, user_id={self.user_id}, token={self.token}, created_at={self.created_at})"

