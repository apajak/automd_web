from uuid import uuid4
from flask import Flask, render_template, request, redirect, url_for, flash, session, jsonify, make_response, send_file
from flask_limiter import Limiter
from functools import wraps
from werkzeug.utils import secure_filename
from flask_limiter.util import get_remote_address
from dotenv import load_dotenv
from historyManager import History
from mysqlManager import MySQLManager as dataBase
import os
from mailManager import PasswordResetEmailSender
from flask_cors import CORS
from loguru import logger
import time
import re
import secrets
import requests
import shutil
import tempfile
import zipfile
import uuid
from datetime import datetime
from statisticsManager import Statistics

from projectManager import Project

app = Flask(__name__, static_url_path='/static')
load_dotenv(".env")
app.secret_key = os.getenv("SECRET_KEY")
host = os.getenv("DATABASE_HOST")
db_user = os.getenv("DATABASE_USER")
db_password = os.getenv("DATABASE_PASSWORD")
db_name = os.getenv("DATABASE_NAME")
# Set a secret key for the session
db = dataBase(host, db_user, db_password, db_name)
current_user = None
# Use a constant or configuration file to store the image and movie directories.
SECRET_KEY = os.getenv("SECRET_KEY")
UPLOAD_MARKDOWNS_DIR = "static/markdowns/"
WORK_DIRS = "static/workdirs/"
PROFILE_PICS_DIR = "static/profile_pics/"
app.config['MDEDITOR_FILE_UPLOADER'] = os.path.join(WORK_DIRS, 'uploads')
ZIP_DOWNLOAD_FOLDER = "static/zip_download/"
ALLOWED_EXTENSIONS = {'png', 'jpg', 'jpeg', 'gif', 'mp4', 'mov', 'avi', 'mkv', 'webm','txt'}
LOG_DIR_ACCESS = "./logs/lexit-cloud-access.log"
LOG_DIR_ERROR = "./logs/lexit-cloud-error.log"
LOG_DIR_SECURITY = "./logs/lexit-cloud-security.log"
LOG_DIR_DEBUG = "./logs/lexit-cloud-debug.log"
LOG_DIR_CRITICAL = "./logs/lexit-cloud-critical.log"
LOG_DIR = "./logs/lexit-cloud.log"
README_FILE = "static/app/README.md"
APP_PATH = "static/app/markdownify_app.zip"
GENERATE_MARKDOWN_API_URL = "https://markdownify.api.arpanode.fr/generate_markdown"
app.config['WORK_DIRS'] = WORK_DIRS
app.config['ALLOWED_EXTENSIONS'] = ALLOWED_EXTENSIONS
app.config['MAX_CONTENT_LENGTH'] = 1000 * 1024 * 1024 # 1 GB
app.config['UPLOAD_MARKDOWNS_DIR'] = UPLOAD_MARKDOWNS_DIR
app.config['ZIP_DOWNLOAD_FOLDER'] = ZIP_DOWNLOAD_FOLDER
app.config['GENERATE_MARKDOWN_API_URL'] = GENERATE_MARKDOWN_API_URL
app.config['README_FILE'] = README_FILE
app.config['APP_PATH'] = APP_PATH
app.config['PROFILE_PICS_DIR'] = PROFILE_PICS_DIR
MAX_LOGIN_ATTEMPTS = 5
login_attempts = {}
blocked_users = {}
# Define the initial block duration in seconds
initial_block_duration = 60  # 1 minute
# Mail server configuration
smtp_server = os.getenv("MAIL_SERVER")
smtp_port = os.getenv("MAIL_PORT")
smtp_sender = os.getenv("MAIL_USERNAME")
smtp_password = os.getenv("MAIL_PASSWORD")
# Create a password reset email sender
password_reset_email_sender = PasswordResetEmailSender(smtp_server, smtp_port, smtp_sender, smtp_password)

# init_stats = Statistics(0, 0, 1, 0, 0, 0, 0, 0, 0, 0)
# db.insert_statistics(init_stats)
# Set up CORS
# Read the whitelist.txt file
with open('white_list.txt') as f:
    whitelist_urls = f.read().splitlines()

# Configure CORS with the whitelist URLs
CORS(app, resources={r"/*": {"origins": whitelist_urls}})

# Set up rate limiting
limiter = Limiter(
    get_remote_address,
    app=app,
    default_limits=["100 per second"],
    storage_uri="redis://localhost:6379"
)

# limiter = Limiter(
#     get_remote_address,
#     app=app,
#     default_limits=["100 per second"],
# )

# Set up logging
logger.level("SECURITY-Lv1", no=15, color="<green>")
logger.level("SECURITY-Lv2", no=15, color="<yellow>")
logger.level("SECURITY-Lv3", no=15, color="<red>")
logger.level("ACCESS-Lv1", no=25, color="<green>")
logger.level("ACCESS-Lv2", no=25, color="<blue>")
logger.level("ACCESS-Lv3", no=25, color="<yellow>")
logger.level("DENIED", no=25, color="<red>")

logger.add(LOG_DIR_ACCESS, level="ACCESS-Lv1", rotation="1 week", compression="zip", enqueue=True, backtrace=True,
           diagnose=True, colorize=True)
logger.add(LOG_DIR_ACCESS, level="ACCESS-Lv2", rotation="1 week", compression="zip", enqueue=True, backtrace=True,
           diagnose=True, colorize=True)
logger.add(LOG_DIR_ACCESS, level="ACCESS-Lv3", rotation="1 week", compression="zip", enqueue=True, backtrace=True,
           diagnose=True, colorize=True)
logger.add(LOG_DIR_ACCESS, level="DENIED", rotation="1 week", compression="zip", enqueue=True, backtrace=True,
           diagnose=True, colorize=True)
logger.add(LOG_DIR_ERROR, level="ERROR", rotation="1 week", compression="zip", enqueue=True, backtrace=True,
           diagnose=True, colorize=True)
logger.add(LOG_DIR_SECURITY, level="SECURITY-Lv1", rotation="1 week", compression="zip", enqueue=True, backtrace=True,
           diagnose=True, colorize=True)
logger.add(LOG_DIR_SECURITY, level="SECURITY-Lv2", rotation="1 week", compression="zip", enqueue=True, backtrace=True,
           diagnose=True, colorize=True)
logger.add(LOG_DIR_SECURITY, level="SECURITY-Lv3", rotation="1 week", compression="zip", enqueue=True, backtrace=True,
           diagnose=True, colorize=True)
logger.add(LOG_DIR_DEBUG, rotation="1 week", compression="zip", enqueue=True, backtrace=True, diagnose=True,
           colorize=True)
logger.add(LOG_DIR_CRITICAL, rotation="1 week", compression="zip", enqueue=True, backtrace=True, diagnose=True,
           colorize=True)
logger.add(LOG_DIR, rotation="1 week", compression="zip", enqueue=True, backtrace=True, diagnose=True, colorize=True)

def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


# Set up error handling
@app.errorhandler(400)
def bad_request(e):
    logger.warning(f"Bad request: {e}")
    logger.log("SECURITY-Lv2", f"Bad request: {e}")
    return render_template('400.html'), 400

@app.errorhandler(403)
def forbidden(e):
    logger.warning(f"Forbidden: {e}")
    logger.log("SECURITY-Lv3", f"Forbidden: {e}")
    return render_template('403.html'), 403

@app.errorhandler(404)
def page_not_found(e):
    logger.warning(f"Page not found: {e}")
    logger.log("SECURITY-Lv1", f"Page not found: {e}")
    # note that we set the 404 status explicitly
    return render_template('404.html'), 404

@app.errorhandler(405)
def method_not_allowed(e):
    logger.warning(f"Method not allowed: {e}")
    logger.log("SECURITY-Lv3", f"Method not allowed: {e}")
    return render_template('405.html'), 405

@app.errorhandler(429)
def ratelimit_handler(e):
    logger.warning(f"Rate limit exceeded: {e}")
    logger.log("SECURITY-Lv3", f"Rate limit exceeded: {e}")
    return render_template('429.html'), 429

@app.errorhandler(500)
def internal_server_error(e):
    logger.critical(f"Internal server error: {e}")
    logger.debug(f"Internal server error: {e}")
    logger.error(f"Internal server error: {e}")
    return render_template('500.html'), 500

@app.errorhandler(502)
def bad_gateway(e):
    logger.critical(f"Bad gateway: {e}")
    logger.debug(f"Bad gateway: {e}")
    logger.error(f"Bad gateway: {e}")
    return render_template('502.html'), 502

@app.errorhandler(503)
def service_unavailable(e):
    logger.critical(f"Service unavailable: {e}")
    logger.debug(f"Service unavailable: {e}")
    return render_template('503.html'), 503

@app.errorhandler(504)
def gateway_timeout(e):
    logger.critical(f"Gateway timeout: {e}")
    logger.debug(f"Gateway timeout: {e}")
    return render_template('504.html'), 504



def login_required(route_function):
    @wraps(route_function)
    def wrapper(*args, **kwargs):
        if session.get('session') is None:
            logger.warning(f"User not logged in")
            logger.log("DENIED", f"User not logged in")
            flash("You must be logged in to access this page", "danger")
            return redirect(url_for('login'))
        return route_function(*args, **kwargs)

    return wrapper


def admin_required(route_function):
    @wraps(route_function)
    def wrapper(*args, **kwargs):
        if session.get('session') is None:
            logger.warning(f"User not logged in")
            logger.log("DENIED", f"User not logged in")
            flash("You must be logged in to access this page", "danger")
            return redirect(url_for('login'))
        user = db.get_user_by_id(session.get('session'))
        if user.role != "admin":
            logger.warning(f"User {user.username} is not an admin")
            logger.log("DENIED", f"User {user.username} is not an admin")
            logger.log("SECURITY-Lv2", f"User {user.username} is not an admin")
            flash("You must be an admin to access this page", "danger")
            return redirect(url_for('home'))
        return route_function(*args, **kwargs)

    return wrapper


def lexit_required(route_function):
    @wraps(route_function)
    def wrapper(*args, **kwargs):
        if session.get('session') is None:
            logger.log("DENIED", "User not logged in")
            flash("You must be logged in to access this page", "danger")
            return redirect(url_for('login'))
        user = db.get_user_by_id(session.get('session'))
        if user.id != 2:
            logger.log("DENIED", f"User {user.username} is not LexIt")
            logger.log("SECURITY-Lv3", f"User {user.username} is not LexIt")
            flash("You must be LexIt to access this page", "danger")
            return redirect(url_for('home'))
        return route_function(*args, **kwargs)

    return wrapper


def is_valid_email(email):
    logger.debug(f"Validating email {email}")
    # Use a regular expression or a library like Flask-WTF to validate email format
    email_regex = r'^[\w\.-]+@[\w\.-]+\.\w+$'
    return re.match(email_regex, email)


def sanitize_username_input(input_string):
    logger.debug(f"Sanitizing username input {input_string}")
    sanitized_string = input_string.strip()  # Remove leading/trailing whitespace
    # Additional sanitization steps for the login username
    sanitized_string = sanitized_string[:20]  # Limit the username length to 20 characters
    sanitized_string = re.sub(r'[^a-zA-Z0-9_-]', '',
                              sanitized_string)  # Remove special characters except underscores and hyphens
    return sanitized_string


def sanitize_input(input_string):
    logger.debug(f"Sanitizing input {input_string}")
    sanitized_string = input_string.strip()  # Remove leading/trailing whitespace
    # Additional sanitization steps for the input
    sanitized_string = sanitized_string[:50]  # Limit the input length to 50 characters
    sanitized_string = re.sub(r'[^a-zA-Z0-9_-]', '',
                              sanitized_string)  # Remove special characters except underscores and hyphens
    return sanitized_string


@app.route('/', methods=['GET'])
def index():
    if session.get('session') is not None:
        user_id = session.get('session')
        logger.debug(f"User {user_id} is already logged in. Redirecting to 'home' page.")
        return redirect(url_for('home'))
    else:
        logger.debug("User accessed the 'index' page.")
        return render_template('index.html')


@app.route('/login', methods=['GET', 'POST'])
@limiter.limit("10/minute, 50/day")
def login():
    user_id = session.get('session')
    if user_id is not None:
        logger.debug(f"User {user_id} is already logged in. Redirecting to 'home' page.")
        return redirect(url_for('home'))

    # Get the client IP address
    client_ip = request.remote_addr

    # Check if the user is currently blocked
    if blocked_users.get(client_ip, 0) > time.time():
        block_end_time = blocked_users[client_ip]
        remaining_time = int(block_end_time - time.time())
        logger.debug(f"IP address {client_ip} is blocked. Remaining time: {remaining_time} seconds")
        flash(f"You are currently blocked. Please try again in {remaining_time} seconds.", "danger")
        return redirect(url_for('index'))

    if request.method == 'POST':
        identifier = request.form['email']
        password = request.form['password']

        # Check if the client IP has reached the maximum number of login attempts
        if login_attempts.get(client_ip, 0) >= MAX_LOGIN_ATTEMPTS:
            over_attempts_value = 1 + login_attempts.get(client_ip, 0) - MAX_LOGIN_ATTEMPTS
            login_attempts[client_ip] = login_attempts.get(client_ip, 0) + 1
            block_duration = initial_block_duration * over_attempts_value
            block_end_time = time.time() + block_duration
            blocked_users[client_ip] = block_end_time

            logger.debug(f"IP address {client_ip} reached the maximum number of login attempts and is blocked for {block_duration} seconds")
            flash(f"You have reached the maximum number of login attempts. You are blocked for {block_duration} seconds.", "danger")
            return redirect(url_for('index'))

        user = db.login(identifier, password)
        if user is not None:
            session['session'] = user.id
            logger.debug(f"User {session.get('session')} login successful")
            flash("You have been logged in successfully", "success")
            return redirect(url_for('home'))
        else:
            # Increment the login attempts counter for the client IP
            login_attempts[client_ip] = login_attempts.get(client_ip, 0) + 1
            logger.debug(f"IP address {client_ip} login failed ({login_attempts[client_ip]}/{MAX_LOGIN_ATTEMPTS} attempts)")
            flash(f"Wrong email or password. Attempt: {login_attempts[client_ip]}/{MAX_LOGIN_ATTEMPTS}", "danger")
            return redirect(url_for('login'))

    # Log when the user accesses the login page
    logger.debug("User accessed the 'login' page.")
    return render_template('login.html', logged_in=False)


@app.route('/register', methods=['GET', 'POST'])
def register():
    logger.debug("User accessed the 'register' page.")
    user_id = session.get('session')
    if user_id is not None:
        user = db.get_user_by_id(user_id)
        name = user.name
        profile_image = user.image
        loged_in = True
        role = user.role
    else:
        name = None
        profile_image = None
        loged_in = False
        role = None

    if request.method == 'POST':
        # Get form data
        username = request.form['username']
        email = request.form['email']
        password = request.form['password']
        confirm_password = request.form['confirm-password']
        profile_image = request.files['profile-image']
        openai_api_key = request.form['openai-api-key']
        workdir = f"{uuid.uuid4().hex}"
        try:
            # create directories
            os.mkdir(f"./static/workdirs/{workdir}")
            os.mkdir(f"./static/workdirs/{workdir}/pics")
            os.mkdir(f"./static/workdirs/{workdir}/history")
            os.mkdir(f"./static/workdirs/{workdir}/markdowns")
            logger.debug(f"Directories created for user {username}: {workdir}")
        except OSError:
            flash("Creation of the directory failed", "danger")
            logger.error(f"Creation of the directory failed")
            logger.debug(f"Creation of the directory failed")
            return redirect(url_for('register'))

        if password != confirm_password:
            flash("Passwords don't match", "danger")
            logger.warning(f"Passwords don't match for user {username}")
            return redirect(url_for('register'))

        if user_check(username, email):
            # Store profile image if provided
            if profile_image.filename != '':
                profile_image.save(os.path.join("./static/profile_pics", profile_image.filename))
                image_path = os.path.join("./static/profile_pics", profile_image.filename)
                logger.log("SECURITY-Lv2", f"User {username} uploaded an image")
                logger.info(f"User {username} uploaded an image")
                logger.debug(f"User {username} uploaded an image")
            else:
                image_path = "./static/profile_pics/person-circle.svg"

            # Insert user data into the database
            db.register(username, password, email, image_path, openai_api_key, workdir)

            last_statistics = db.get_last_statistics()
            if db.last_statistics_is_today(last_statistics):
                last_statistics.users_count += 1
                db.update_statistics(last_statistics)
            else:
                last_statistics.users_count += 1
                db.insert_statistics(last_statistics)

            flash("You have been registered successfully", "success")
            logger.info(f"User {username} registered successfully")
            return redirect(url_for('login'))
        else:
            logger.error(f"Username or email already taken for user {username}")
            logger.debug(f"Username or email already taken for user {username}")
            flash("Username or email already taken", "danger")
            return redirect(url_for('register'))

    return render_template('register.html', name=name, profile_image_path=profile_image, logged_in=loged_in, role=role)


def user_check(username, email):
    logger.debug(f"Checking user: {username}, email: {email}")

    # Check if username is valid (only letters and numbers, 3 to 20 characters)
    if not re.match("^[a-zA-Z0-9]{3,20}$", username):
        logger.debug(f"Invalid username format: {username}")
        return False

    # Check if email is valid
    if not re.match("^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$", email):
        logger.debug(f"Invalid email format: {email}")
        return False

    # Check if username or email is already taken
    if db.user_exists(username, email):
        logger.debug(f"Username or email already taken: {username}, {email}")
        return False

    logger.debug(f"User check passed for username: {username}, email: {email}")
    return True


@app.route('/home', methods=['GET', 'POST'])
@login_required
def home():
    user_id = session.get('session')
    # if session Get the user from the database
    user = db.get_user_by_id(user_id)
    if user is None:
        # Log user access denial
        logger.debug(f"User {user_id} is not logged in")
        logger.log("DENIED", f"User {user_id} is not logged in")
        logger.log("SECURITY-Lv2", f"User {user_id} is not logged in for home")
        return redirect(url_for('login'))
    else:
        logger.debug(f"User {user_id} is logged in")
        logger.log("ACCESS-Lv1", f"User {user_id} is logged in")
        logger.log("SECURITY-Lv1", f"User {user_id} is logged in for home")
    return render_template('home.html', name=user.name, profile_image_path=user.profile_image,
                           logged_in=True, role=user.role, user=user)


@app.route('/logout')
@login_required
def logout():
    logger.debug(f"User {session.get('session')} logged out")
    logger.log("ACCESS-Lv1", f"User {session.get('session')} logged out")
    session.clear()
    flash("You have been logged out", "info")
    return redirect(url_for('index'))

@app.route('/generate_api_key', methods=['GET', 'POST'])
@login_required
def generate_api_key():
    user_id = session.get('session')
    try:
        # Log the user's action
        logger.debug(f"User {user_id} is generating a new API key")
        logger.log("ACCESS-Lv1", f"User {user_id} is generating a new API key")
        # Generate a new API key using the secrets module
        new_api_key = secrets.token_urlsafe(16)
        # Return the new API key as JSON response
        return jsonify({'new_api_key': new_api_key})
    except Exception as e:
        # Log any exceptions that occur
        logger.error(f"Error in generate_api_key route: {str(e)}")
        # You can also log a traceback for detailed debugging
        logger.debug("Traceback:", exc_info=True)
        # Handle the exception as needed (e.g., return an error response)
        return jsonify({'error': 'API key generation failed'}), 500


@app.route('/profile', methods=['GET', 'POST'])
@login_required
def profile():
    user_id = session.get('session')
    user = db.get_user_by_id(user_id)

    if user is None:
        # Log user access denial
        logger.debug(f"User {user_id} is not logged in")
        logger.log("DENIED", f"User {user_id} is not logged in")
        logger.log("SECURITY-Lv2", f"User {user_id} is not logged in for profile")
        return redirect(url_for('login'))

    identification_token = db.insert_token(user.id)

    if request.method == 'POST':
        # Get form data
        logger.log("SECURITY-Lv2", f"User {user_id} profile post request")
        username = request.form['username']
        email = request.form['email']
        openai_api_key = request.form['openaiapikey']
        api_key = request.form['apikey']
        current_password = request.form['current-password']
        password = request.form['password']
        confirm_password = request.form['confirm-password']
        profile_image = request.files['profile-image']

        try:
            # Validate current password
            if not db.login(user.email, current_password):
                # Log password validation failure
                logger.debug(f"User {user_id} entered the wrong current password")
                logger.log("DENIED", f"User {user_id} entered the wrong current password")
                logger.log("SECURITY-Lv2", f"User {user_id} entered the wrong current password")
                flash("Wrong current password", "danger")
                return redirect(url_for('profile'))

            # Update profile fields
            user.username = sanitize_username_input(username.strip())
            user.api_key = api_key.strip()
            user.openai_api_key = openai_api_key.strip()

            if is_valid_email(email):
                user.email = email.strip()
            else:
                flash("Invalid email", "danger")
                # Log email validation failure
                logger.debug(f"User {user_id} entered an invalid email")
                logger.log("DENIED", f"User {user_id} entered an invalid email")
                logger.log("SECURITY-Lv2", f"User {user_id} entered an invalid email")
                return redirect(url_for('profile'))

            if profile_image.filename != '':
                # Validate and handle profile image upload
                allowed_extensions = {'jpg', 'jpeg', 'png', 'gif', 'webp'}
                if '.' in profile_image.filename and profile_image.filename.rsplit('.', 1)[
                    1].lower() in allowed_extensions:
                    filename = secure_filename(profile_image.filename)
                    profile_image_path = os.path.join("./static/profile_pics", filename)
                    profile_image.save(profile_image_path)
                    # Replace '\' by '/' before storing the path in the database
                    profile_image_path = profile_image_path.replace('\\', '/')
                    user.image = profile_image_path
                else:
                    flash("Invalid profile image file", "danger")
                    # Log profile image validation failure
                    logger.debug(f"User {user_id} uploaded an invalid profile image file")
                    logger.log("DENIED", f"User {user_id} uploaded an invalid profile image file")
                    logger.log("SECURITY-Lv2", f"User {user_id} uploaded an invalid profile image file")
                    return redirect(url_for('profile'))

            # Update user in the database
            db.update_user(user)

            # Log successful profile update
            logger.debug(f"User {user_id} profile updated successfully")
            logger.info(f"User {user_id} profile updated successfully")
        except Exception as e:
            # Log database update errors
            flash("Username or email already exists", "danger")
            # Log username or email duplication error
            logger.debug(f"User {user_id} username or email already exists")
            logger.log("DENIED", f"User {user_id} username or email already exists")
            logger.log("SECURITY-Lv1", f"User {user_id} username or email already exists")
            # Log the database update error
            logger.error(f"User {user_id} username or email already exists: {e}")
            return redirect(url_for('profile'))

        # Update password if provided
        if password.strip() != '':
            if password != confirm_password:
                flash("Passwords don't match", "danger")
                # Log password mismatch error
                logger.debug(f"User {user_id} passwords don't match")
                logger.log("DENIED", f"User {user_id} passwords don't match")
                return redirect(url_for('profile'))
            else:
                db.update_user_password(password, user_id)
                # Log password update
                logger.debug(f"User {user_id} password updated successfully")
                logger.info(f"User {user_id} password updated successfully")
                flash("Password updated successfully", "success")

        flash("Profile updated successfully", "success")
        # Log successful profile update
        logger.debug(f"User {user_id} profile updated successfully")
        logger.info(f"User {user_id} profile updated successfully")
        return redirect(url_for('home'))

    return render_template('profile.html', user=user, name=user.name, profile_image_path=user.profile_image,
                           logged_in=True, role=user.role, identification_token=identification_token)


@app.route('/files', methods=['GET', 'POST'])
@login_required
def files():
    user = db.get_user_by_id(session.get('session'))

    try:
        identification_token = db.insert_token(user.id)
    except Exception as e:
        # Log any errors when inserting the token
        logger.error(f"Error while inserting token: {e}")
        identification_token = None

    if user is None:
        # Log user access denial
        logger.debug(f"User {session.get('session')} is not logged in")
        logger.log("DENIED", f"User {session.get('session')} is not logged in")
        logger.log("SECURITY-Lv2", f"User {session.get('session')} is not logged in for markdowns")
        return redirect(url_for('login'))

    # Log user access and identification token generation
    logger.debug(f"User {user.name} gets the markdowns page")
    logger.log("ACCESS-Lv1", f"User {session.get('session')} is logged in")

    user_history_files = db.get_history_by_user_id(user.id)
    user_markdowns_files = db.get_projects_by_user_id(user.id)

    return render_template('files.html', user=user, name=user.name,
                           profile_image_path=user.profile_image, logged_in=True, role=user.role,
                           history_files=user_history_files, markdown_files=user_markdowns_files,
                           identification_token=identification_token)


@app.route('/remove_history/<int:file_id>')
def remove_file(file_id):
    session_token = request.args.get('identification_token')

    try:
        if not db.check_token(session_token):
            flash("Your token has expired", "warning")
            return redirect(url_for('files'))
    except Exception as e:
        # Log any errors when checking the token
        logger.error(f"Error while checking token: {e}")

    try:
        user_id = db.get_userid_by_token(session_token)
    except Exception as e:
        # Log any errors when getting the user ID by token
        logger.error(f"Error while getting user ID by token: {e}")
        user_id = None

    try:
        db.remove_token(session_token)
    except Exception as e:
        # Log any errors when removing the token
        logger.error(f"Error while removing token: {e}")

    if user_id is None:
        # Log user access denial
        flash("You are not logged in", "danger")
        logger.debug("User is not logged in")
        return redirect(url_for('login'))

    user = db.get_user_by_id(user_id)
    if user is None:
        # Log user access denial
        flash("You are not logged in", "danger")
        logger.debug("User is not logged in")
        return redirect(url_for('login'))

    history = db.get_history_by_id(file_id)
    if history.user_id != user.id:
        # Log unauthorized access attempt
        flash("You are not the owner of the file", "danger")
        logger.debug(f"User {user.name} attempted to remove a file owned by another user")
        return redirect(url_for('files'))

    try:
        db.delete_history(history.id)

        last_statistics = db.get_last_statistics()
        if db.last_statistics_is_today(last_statistics):
            last_statistics.histories_count -= 1
            db.update_statistics(last_statistics)
        else:
            last_statistics.histories_count -= 1
            db.insert_statistics(last_statistics)

        # Log successful file removal
        logger.debug(f"User {user.name} removed file ID {file_id} successfully")
        logger.info(f"User {user.name} removed file ID {file_id} successfully")
        flash("File removed successfully", "success")
        return redirect(url_for('files'))

    except Exception as e:
        # Log errors when removing the file
        flash("Error while removing file", "danger")
        logger.error(f"Error while removing file: {e}")
        return redirect(url_for('files'))


@app.route('/remove_markdown/<int:file_id>')
def remove_markdown(file_id):
    session_token = request.args.get('identification_token')
    try:
        if not db.check_token(session_token):
            flash("Your token has expired", "warning")
            return redirect(url_for('files'))
    except Exception as e:
        # Log any errors when checking the token
        logger.error(f"Error while checking token: {e}")

    try:
        user_id = db.get_userid_by_token(session_token)
    except Exception as e:
        # Log any errors when getting the user ID by token
        logger.error(f"Error while getting user ID by token: {e}")
        user_id = None

    try:
        db.remove_token(session_token)
    except Exception as e:
        # Log any errors when removing the token
        logger.error(f"Error while removing token: {e}")

    if user_id is None:
        # Log user access denial
        flash("You are not logged in", "danger")
        logger.debug("User is not logged in")
        return redirect(url_for('login'))

    user = db.get_user_by_id(user_id)
    if user is None:
        # Log user access denial
        flash("You are not logged in", "danger")
        logger.debug("User is not logged in")
        return redirect(url_for('login'))

    markdown = db.get_project_by_id(file_id)
    # Check if user is the owner of the file
    if markdown.owner_user_id != user.id:
        # Log unauthorized access attempt
        flash("You are not the owner of the file", "danger")
        logger.debug(f"User {user.name} attempted to remove a file owned by another user")
        return redirect(url_for('files'))

    try:
        db.delete_project(markdown.id)

        last_statistics = db.get_last_statistics()
        if db.last_statistics_is_today(last_statistics):
            last_statistics.markdowns_count -= 1
            db.update_statistics(last_statistics)
        else:
            last_statistics.markdowns_count -= 1
            db.insert_statistics(last_statistics)

        # Log successful file removal
        logger.debug(f"User {user.name} removed Markdown file ID {file_id} successfully")
        logger.info(f"User {user.name} removed Markdown file ID {file_id} successfully")
        flash("File removed successfully", "success")
        return redirect(url_for('files'))

    except Exception as e:
        # Log errors when removing the file
        flash("Error while deleting project", "danger")
        logger.error(f"Error while removing Markdown file: {e}")
        return redirect(url_for('files'))


@app.route('/remove_user', methods=['POST'])
@admin_required
def remove_user():
    logger.debug(f"User {session.get('session')} delete user")
    logger.log("ACCESS-Lv2", f"User {session.get('session')} delete user")
    logger.log("SECURITY-Lv2", f"User {session.get('session')} delete user")

    if request.method == 'POST':
        user_id = request.args.get('id')  # Use request.args to retrieve URL parameters
        verification_token = request.args.get('token')
        token_user_id = db.get_userid_by_token(verification_token)
        token_user = db.get_user_by_id(token_user_id)

        if token_user is None or token_user.role != "admin":
            # Log unauthorized access attempt
            logger.critical(f"User {session.get('session')} tried to delete user {user_id} without admin privileges")
            logger.log("SECURITY-Lv3",
                       f"User {session.get('session')} tried to delete user {user_id} without admin privileges")
            flash("You can't delete a user. But LexIt can delete you!", "danger")
            return redirect(url_for('home'))
        else:
            # Renew the token
            db.remove_token(verification_token)

        try:
            user = db.get_user_by_id(user_id)
            user_workdir = user.workdir

            if user.id == 2:
                # Log an attempt to delete LexIt
                logger.critical(f"User {session.get('session')} tried to delete LexIt")
                logger.log("SECURITY-Lv3", f"User {session.get('session')} tried to delete LexIt")
                flash("You can't delete LexIt. But he can delete you!", "danger")
                return redirect(url_for('home'))

            if db.delete_user(user_id):
                # Delete the user directory
                shutil.rmtree(f"./static/workdirs/{user_workdir}")
                last_statistics = db.get_last_statistics()

                if db.last_statistics_is_today(last_statistics):
                    last_statistics.users_count -= 1
                    db.update_statistics(last_statistics)
                else:
                    last_statistics.users_count -= 1
                    db.insert_statistics(last_statistics)

                # Log successful user deletion
                logger.debug(f"User {user.name} deleted successfully")
                logger.log("ACCESS-Lv2", f"User {user.name} deleted successfully")
                logger.log("SECURITY-Lv1", f"User {user.name} deleted successfully")
                logger.info(f"User {user.name} deleted successfully")
                flash(f"User {user.name} deleted successfully", "success")
                return redirect(url_for('admin'))
            else:
                # Log user not deleted
                logger.debug(f"User {user.name} user not deleted")
                logger.log("ACCESS-Lv2", f"User {user.name} user not deleted")
                logger.log("SECURITY-Lv1", f"User {user.name} user not deleted")
                logger.info(f"User {user.name} user not deleted")
                flash(f"User {user.name} user not deleted", "danger")
                return redirect(url_for('admin'))
        except Exception as e:
            # Log errors when deleting the user
            flash("User not deleted", "danger")
            logger.debug(f"User {session.get('session')} user not deleted")
            logger.error(f"User {session.get('session')} user not deleted: {e}")
            return redirect(url_for('admin'))

    return redirect(url_for('admin'))


@app.route('/download_history/<int:file_id>')
def download_file(file_id):
    session_token = request.args.get('identification_token')

    try:
        if not db.check_token(session_token):
            flash("Your token has expired", "warning")
            return redirect(url_for('files'))
    except Exception as e:
        logger.error(f"Error while checking token: {e}")

    try:
        user_id = db.get_userid_by_token(session_token)
    except Exception as e:
        logger.error(f"Error while getting user id by token: {e}")
        user_id = None

    if user_id is None:
        flash("You are not logged in", "danger")
        return redirect(url_for('login'))

    user = db.get_user_by_id(user_id)
    if user is None:
        flash("You are not logged in", "danger")
        return redirect(url_for('login'))

    history = db.get_history_by_id(file_id)
    if history.user_id != user.id:
        flash("You are not the owner of the file", "danger")
        return redirect(url_for('files'))

    content = history.content
    filename = f"{history.title}"

    try:
        temp_file = tempfile.NamedTemporaryFile(delete=False)
        temp_file.write(content.encode())
        temp_file.close()

        last_statistics = db.get_last_statistics()
        if db.last_statistics_is_today(last_statistics):
            last_statistics.histories_downloaded += 1
            db.update_statistics(last_statistics)
        else:
            last_statistics.histories_downloaded += 1
            db.insert_statistics(last_statistics)

        # Log successful file download
        logger.info(f"User {user.name} downloaded file: {filename}")
        return send_file(temp_file.name, as_attachment=True, download_name=filename)
    except Exception as e:
        # Log download failure
        logger.error(f"User {user.name} failed to download file: {e}")
        flash("Failed to download file", "danger")
        return redirect(url_for('files'))
    finally:
        try:
            os.unlink(temp_file.name)
        except Exception as e:
            # Log error while deleting temporary file
            logger.error(f"Error while deleting temporary file: {e}")
            logger.debug("Traceback:", exc_info=True)


@app.route('/download_markdown/<int:file_id>')
def download_markdown(file_id):
    session_token = request.args.get('identification_token')

    try:
        if not db.check_token(session_token):
            flash("Your token has expired", "warning")
            return redirect(url_for('files'))
    except Exception as e:
        logger.error(f"Error while checking token: {e}")

    try:
        user_id = db.get_userid_by_token(session_token)
    except Exception as e:
        logger.error(f"Error while getting user id by token: {e}")
        user_id = None

    if user_id is None:
        flash("You are not logged in", "danger")
        return redirect(url_for('login'))

    user = db.get_user_by_id(user_id)
    if user is None:
        flash("You are not logged in", "danger")
        return redirect(url_for('login'))

    markdown = db.get_project_by_id(file_id)
    history = db.get_history_by_id(markdown.history_id)

    # check if user is the owner of the file
    if markdown.owner_user_id != user.id:
        flash("You are not the owner of the file", "danger")
        return redirect(url_for('files'))

    content = markdown.content
    filename = f"{markdown.title}".split('.')[0]
    media = markdown.media

    try:
        # Create temporary directories and files
        temp_dir = tempfile.mkdtemp(prefix=f"{filename}_")
        markdown_dir = os.path.join(temp_dir, f"markdown_{filename}")
        pics_dir = os.path.join(markdown_dir, "pics")
        os.makedirs(pics_dir)
        history_dir = os.path.join(markdown_dir, f"history_{history.title.split('.')[0]}")
        os.makedirs(history_dir)
        history_path = os.path.join(history_dir, f"{history.title}")
        with open(history_path, 'w') as history_file:
            history_file.write(history.content)
        if media:
            media_files = media.split(';')
            for media_file in media_files:
                user_workdir = os.path.join(app.config['WORK_DIRS'], user.workdir)
                user_pics_dir = os.path.join(user_workdir, 'pics')
                media_file_path = os.path.join(user_pics_dir, media_file)
                shutil.copy(media_file_path, pics_dir)
        markdown_path = os.path.join(markdown_dir, f"{filename}.md")
        with open(markdown_path, 'w') as markdown_file:
            markdown_file.write(content)

        # Create a zip file containing the directory in user's workdir
        user_workdir = os.path.join(app.config['WORK_DIRS'], user.workdir)
        user_zip_dir = os.path.join(user_workdir, "zip_download")
        datetime_str = datetime.now().strftime("%Y-%m-%d")
        str_filename = filename.split('.')[0].replace(' ', '_')
        zip_path = os.path.join(user_zip_dir, f"{datetime_str}_markdown_{str_filename}.zip")
        with zipfile.ZipFile(zip_path, 'w', zipfile.ZIP_DEFLATED) as zipf:
            for root, dirs, files in os.walk(temp_dir):
                for file in files:
                    file_path = os.path.join(root, file)
                    arcname = os.path.relpath(file_path, temp_dir)
                    zipf.write(file_path, arcname=arcname)

        # Clean up temporary directory
        shutil.rmtree(temp_dir)

        # Update statistics
        last_statistics = db.get_last_statistics()
        if db.last_statistics_is_today(last_statistics):
            last_statistics.markdowns_downloaded += 1
            db.update_statistics(last_statistics)
        else:
            last_statistics.markdowns_downloaded += 1
            db.insert_statistics(last_statistics)

        # Log successful file download
        logger.info(f"User {user.name} downloaded markdown file: {filename}")

        # Send the zip file for download
        return send_file(zip_path, as_attachment=True)

    except Exception as e:
        # Log download failure
        logger.error(f"User {user.name} failed to download markdown file: {e}")
        flash("An error occurred while preparing the download", "danger")
        return redirect(url_for('files'))

    finally:
        # Clean up zip file
        try:
            os.remove(zip_path)
            logger.debug(f"User {user.name} zip file removed successfully")
        except Exception as e:
            # Log error while removing zip file
            logger.error(f"Error while removing zip file: {e}")
            logger.debug("Traceback:", exc_info=True)


@app.route('/history_editor/<int:file_id>', methods=['GET', 'POST'])
@login_required
def history_editor(file_id):
    user_id = session.get('session')
    identification_token = request.args.get('identification_token')

    try:
        user = db.get_user_by_id(user_id)
    except Exception as e:
        logger.error(f"Error while getting user by ID: {e}")
        user = None

    if user is None:
        flash("You are not logged in", "danger")
        return redirect(url_for('login'))

    history = db.get_history_by_id(file_id)

    if history is None:
        flash("File not found", "danger")
        logger.warning(f"User {user.name} attempted to edit non-existing file (ID: {file_id})")
        return redirect(url_for('files'))
    elif history.user_id != user.id:
        flash("You are not the owner of the file", "danger")
        logger.warning(f"User {user.name} attempted to edit file (ID: {file_id}) owned by another user")
        return redirect(url_for('files'))

    if request.method == 'POST':
        title = request.form.get('title')
        content = request.form.get('content')
        identification_token = request.form.get('identification_token')

        try:
            if not db.check_token(identification_token):
                flash("Your token has expired", "warning")
                return redirect(url_for('files'))
        except Exception as e:
            logger.error(f"Error while checking token: {e}")

        # Update the file in the database
        history.title = title
        history.content = content

        try:
            db.update_history(history)
            flash("File updated successfully", "success")
            logger.info(f"User {user.name} updated history file (ID: {file_id}) successfully")
            return redirect(url_for('history_editor', file_id=file_id, identification_token=identification_token))
        except Exception as e:
            flash("Failed to update file", "danger")
            logger.error(f"User {user.name} failed to update history file (ID: {file_id}): {e}")
            return redirect(url_for('files'))

    return render_template('history_editor.html', history=history, user=user, name=user.name, profile_image_path=user.profile_image,
                           logged_in=True, role=user.role, identification_token=identification_token)


def cleanup_user_pics(user_id):
    user = db.get_user_by_id(user_id)
    if user is None:
        logger.warning(f"User cleanup for user ID {user_id} failed: User not found")
        return
    user_workdir = os.path.join(app.config['WORK_DIRS'], user.workdir)
    user_pics_dir = os.path.join(user_workdir, 'pics')
    # User projects
    user_projects = db.get_projects_by_user_id(user.id)
    media_list = []
    for project in user_projects:
        if project.media:
            media_list = media_list + project.media.split(';')
    # If in user pics dir there is a file that is not in the media list, remove it
    for file in os.listdir(user_pics_dir):
        if file not in media_list:
            file_path = os.path.join(user_pics_dir, file)
            os.remove(file_path)
            logger.info(f"Removed unused file '{file}' from user ID {user_id}'s pics directory")


@app.route('/markdown_editor/<int:file_id>', methods=['GET', 'POST'])
@login_required
def markdown_editor(file_id):
    # If session, get the user from the database
    user_id = session.get('session')
    identification_token = request.args.get('identification_token')

    try:
        user = db.get_user_by_id(user_id)
    except Exception as e:
        logger.error(f"Error while getting user by ID: {e}")
        user = None

    if user is None:
        flash("You are not logged in", "danger")
        logger.warning(f"User attempted to access markdown editor without being logged in")
        return redirect(url_for('login'))

    markdown = db.get_project_by_id(file_id)

    if markdown is None:
        flash("File not found", "danger")
        logger.warning(f"User {user.name} attempted to edit non-existing file (ID: {file_id})")
        return redirect(url_for('files'))
    elif markdown.owner_user_id != user.id:
        flash("You are not the owner of the file", "danger")
        logger.warning(f"User {user.name} attempted to edit file (ID: {file_id}) owned by another user")
        return redirect(url_for('files'))

    # Get work directories and project directories
    workdir = os.path.join(app.config['WORK_DIRS'], user.workdir)
    pics_dir = os.path.join(workdir, 'pics')
    markdowns_dir = os.path.join(workdir, 'markdowns')
    project_name = markdown.title.split('.')[0]
    project_dir = os.path.join(markdowns_dir, project_name)
    project_pics_dir = os.path.join(project_dir, 'pics')

    # Create or verify project directories
    if not os.path.exists(project_dir):
        os.mkdir(project_dir)
    if not os.path.exists(project_pics_dir):
        os.mkdir(project_pics_dir)

    # Copy stored media files into the project pics directory
    if markdown.media:
        media_files = markdown.media.split(';')
        for media_file in media_files:
            media_file_path = os.path.join(pics_dir, media_file)
            try:
                shutil.copy(media_file_path, project_pics_dir)
            except Exception as e:
                logger.error(f"Error while copying media file: {e}")
                logger.debug("Traceback:", exc_info=True)
                filename = media_file_path.split('/')[-1]
                flash(f"Error while copying media file: {filename}", "danger")
                continue
    else:
        media_files = []

    if request.method == 'POST':
        title = request.form.get('title')
        content = request.form.get('content')
        media_files = os.listdir(project_pics_dir)
        identification_token = request.form.get('identification_token')

        try:
            if not db.check_token(identification_token):
                flash("Your token has expired", "warning")
                return redirect(url_for('files'))
        except Exception as e:
            logger.error(f"Error while checking token: {e}")

        # For files in the pics directory, check if they are already in the media folder, and copy if not
        if media_files:
            for media_file in media_files:
                media_file_path = os.path.join(project_pics_dir, media_file)
                if markdown.media:
                    if media_file not in markdown.media:
                        shutil.copy(media_file_path, pics_dir)

        # Update the media list
        media_paths_str = ';'.join(media_files)

        # Check if the data has changed before updating
        if markdown.title != title or markdown.content != content or markdown.media != media_paths_str:
            markdown.title = title
            markdown.content = content
            markdown.media = media_paths_str

            try:
                db.update_project(markdown)
                flash("Markdown updated successfully", "success")
                logger.info(f"User {user.name} updated markdown file (ID: {file_id}) successfully")
                return redirect(url_for('markdown_editor', file_id=file_id, identification_token=identification_token))
            except Exception as e:
                flash("Failed to update markdown", "danger")
                logger.error(f"User {user.name} failed to update markdown file (ID: {file_id}): {e}")
                return redirect(url_for('files'))
            finally:
                # Remove the project directory
                shutil.rmtree(project_dir)
                cleanup_user_pics(user.id)
        else:
            flash("No changes detected, no update necessary", "info")
            # Remove the project directory
            shutil.rmtree(project_dir)
            cleanup_user_pics(user.id)
            return redirect(url_for('markdown_editor', file_id=file_id, identification_token=identification_token))

    return render_template('markdown_editor.html', markdown=markdown, user=user, name=user.name,
                            profile_image_path=user.profile_image, logged_in=True, role=user.role, pics_dir=pics_dir,
                            workdir=workdir, project_dir=project_dir, project_pics_dir=project_pics_dir,
                            identification_token=identification_token)


@app.route('/get_media_list', methods=['GET'])
def get_media_list():
    try:
        # Get the project pics directory from the request
        project_pics_dir = request.args.get('project_pics_dir')
        media_files = os.listdir(project_pics_dir)

        # Log the request and the retrieved media files
        logger.info(f"Received request to get media list from directory: {project_pics_dir}")
        logger.info(f"Retrieved media files: {media_files}")

        return jsonify(media_files)
    except Exception as e:
        logger.error(f"Error in get_media_list route: {e}")
        return jsonify(error=str(e)), 500

@app.route('/remove_media', methods=['POST'])
def remove_media():
    try:
        # Get the project pics directory and identification token from the request
        project_pics_dir = request.form.get('project_pics_dir')
        identification_token = request.form.get('identification_token')

        # Check the validity of the token
        valid_token = db.check_token(identification_token)
        if valid_token:
            # Get the filename to be removed from the request
            filename = request.form.get('filename')
            file_path = os.path.join(project_pics_dir, filename)

            # Check if the file exists before attempting to remove it
            if os.path.exists(file_path):
                os.remove(file_path)

                # Remove the filename from the media list in the database
                workdir = file_path.split('/')[2]
                project_name = f"{file_path.split('/')[3]}.md"
                user = db.get_user_by_workdir(workdir)
                markdown = db.get_user_project_by_title(user.id, project_name)
                media_list = markdown.media.split(';')

                # If the file is in the media list, remove it
                if filename in media_list:
                    media_list.remove(filename)
                    media_list_str = ';'.join(media_list)
                    db.update_project_media(markdown.id, media_list_str)

                # Log the successful removal of the file
                logger.info(f"Removed file '{filename}' from directory: {project_pics_dir}")

                return "OK"
            else:
                # Log and return an error if the file is not found
                logger.error(f"File not found: {file_path}")
                return "File not found", 404
        else:
            # Log and return an error if the token is invalid
            logger.warning("Invalid token in remove_media route")
            return "TokenExpired", 403
    except Exception as e:
        # Log and return an error if an exception occurs
        logger.error(f"Error in remove_media route: {e}")
        return jsonify(error=str(e)), 500


@app.route('/upload_media', methods=['POST'])
def upload_media():
    try:
        project_pics_dir = request.args.get('project_pics_dir')
        if not project_pics_dir:
            return "Missing workdir parameter", 400

        media_file = request.files.get('media')
        if media_file:
            filename = secure_filename(media_file.filename)
            file_path = os.path.join(project_pics_dir, filename)
            media_file.save(file_path)

            # Log the uploaded media file and its destination path
            logger.info(f"Uploaded media file '{filename}' to: {file_path}")

            return file_path
        return "Error uploading media", 400
    except Exception as e:
        logger.error(f"Error in upload_media route: {e}")
        return jsonify(error=str(e)), 500


@app.route('/save_history', methods=['POST'])
def save_history():
    try:
        api_key = request.form.get('api_key')
        if not api_key:
            return "Missing api_key parameter", 400
        else:
            user = db.get_user_by_api_key(api_key)
            if user is None:
                return "Invalid api_key", 403
            else:
                # Get the file from the request
                uploaded_file = request.files['history_file']
                if uploaded_file.filename == '':
                    return "No file selected", 400
                if uploaded_file and allowed_file(uploaded_file.filename):
                    filename = secure_filename(uploaded_file.filename)
                    uuid = str(uuid4())
                    filename = uuid + "_" + filename
                    temp_dir = tempfile.mkdtemp(prefix=f"{user.workdir}_")
                    file_path = os.path.join(temp_dir, filename)
                    uploaded_file.save(file_path)

                    # Log the uploaded history file and its destination path
                    logger.info(f"Uploaded history file '{filename}' to: {file_path}")

                    # Read the file content
                    with open(file_path, 'r') as file:
                        content = file.read()
                    # Get the title from the request
                    title = request.form.get('title')
                    # Create a history object
                    history = History(id=None, title=title, user_id=user.id, content=content, created_at=None)
                    # Insert the history into the database
                    try:
                        history_id = db.insert_history(history)

                        # Update statistics
                        last_statistics = db.get_last_statistics()
                        if db.last_statistics_is_today(last_statistics):
                            last_statistics.histories_count += 1
                            last_statistics.histories_uploaded += 1
                            db.update_statistics(last_statistics)
                        else:
                            last_statistics.histories_count += 1
                            last_statistics.histories_uploaded += 1
                            db.insert_statistics(last_statistics)

                        # Remove the file from the server after inserting it into the database
                        os.remove(file_path)
                        response_data = {"history_id": history_id}
                        return jsonify(response_data), 200
                    except Exception as e:
                        return "Error while inserting history", 500
    except Exception as e:
        logger.error(f"Error in save_history route: {e}")
        return jsonify(error=str(e)), 500


@app.route('/generate_markdown', methods=['GET', 'POST'])
def generate_markdown():
    try:
        api_key = request.form.get('api_key')
        history_id = request.form.get('history_id')
        if not api_key:
            return "Missing api_key parameter", 400
        user = db.get_user_by_api_key(api_key)
        if user is None:
            return "Invalid api_key", 403
        else:
            history = db.get_history_by_id(history_id)
            if history is None:
                return "Invalid history_id", 400
            if history.user_id != user.id:
                return "You are not the owner of the file", 403
            else:
                # create history file
                temp_dir = tempfile.mkdtemp(prefix=f"{user.workdir}_")
                history_path = os.path.join(temp_dir, f"{history.title}.txt")
                with open(history_path, 'w') as history_file:
                    history_file.write(history.content)
                # generate markdown file
                generate_markdown_api_url = app.config['GENERATE_MARKDOWN_API_URL']

                # Log the generation process
                logger.info(f"Generating markdown for history ID {history_id} by user {user.id}")

                # send file and api key to the API
                files = {'file': open(history_path, 'rb')}
                data = {'api_key': api_key}
                try:
                    response = requests.post(generate_markdown_api_url, files=files, data=data)
                    if response.status_code == 200:
                        # save markdown file
                        markdown_content = response.text
                        if history.title.endswith('.txt'):
                            markdown_title = f"{history.title[:-4]}.md"
                        else:
                            markdown_title = f"{history.title}.md"
                        markdown = Project(id=None, title=markdown_title, owner_user_id=user.id, history_id=history.id,
                                            content=markdown_content, media=None, created_at=None)
                        markdown_id = db.insert_project(markdown)
                        # remove the history file
                        os.remove(history_path)

                        # Log successful markdown generation
                        logger.info(f"Markdown generated successfully for history ID {history_id} by user {user.id}")

                        # return markdown editor URL
                        user_token = db.renew_token(user.id)
                        markdown_editor_url = url_for('markdown_editor', file_id=markdown_id, identification_token=user_token, _external=True)
                        response_data = {"markdown_url": markdown_editor_url}
                        flash("Markdown generated successfully", "success")

                        # Update statistics
                        last_statistics = db.get_last_statistics()
                        if db.last_statistics_is_today(last_statistics):
                            last_statistics.markdowns_count += 1
                            db.update_statistics(last_statistics)
                        else:
                            last_statistics.markdowns_count += 1
                            db.insert_statistics(last_statistics)

                        return jsonify(response_data), 200
                    else:
                        flash("Error while generating markdown", "danger")
                        return "Error while generating markdown", 500
                except Exception as e:
                    flash("Error while generating markdown", "danger")
                    logger.error(f"Error while generating markdown: {e}")
                    return "Error while generating markdown", 500
    except Exception as e:
        flash("Error in generate_markdown route", "danger")
        logger.error(f"Error in generate_markdown route: {e}")
        return jsonify(error=str(e)), 500


@app.route('/markdownify_app', methods=['GET'])
@login_required
def markdown_app():
    try:
        user_id = session.get('session')
        # if session Get the user from the database
        if user_id is None:
            flash("You are not logged in", "danger")
            return redirect(url_for('login'))
        else:
            user = db.get_user_by_id(user_id)
        if user is None:
            flash("You are not logged in", "danger")
            return redirect(url_for('login'))

        # Log access to the markdownify app route
        logger.info(f"User {user.id} accessed the markdownify app")

        return render_template('app.html',
                               user=user,
                               name=user.name if user else None,
                               profile_image_path=user.profile_image if user else None,
                               logged_in=user is not None,
                               role=user.role if user else None
                               )
    except Exception as e:
        flash("Error in markdownify_app route", "danger")
        logger.error(f"Error in markdownify_app route: {e}")
        return jsonify(error=str(e)), 500


@app.route('/readme', methods=['GET'])
@login_required
def readme():
    try:
        user_id = session.get('session')
        # if session Get the user from the database
        user = db.get_user_by_id(user_id)
        if user is None:
            flash("You are not logged in", "danger")
            return redirect(url_for('login'))
        # open readme file
        readme_path = app.config['README_FILE']
        with open(readme_path, 'r') as readme_file:
            readme_content = readme_file.read()

        # Log access to the readme route
        logger.info(f"User {user.id} accessed the readme")

        return render_template('readme.html', readme_content=readme_content, user=user, name=user.name,
                               profile_image_path=user.profile_image, logged_in=True, role=user.role)
    except Exception as e:
        flash("Error in readme route", "danger")
        logger.error(f"Error in readme route: {e}")
        return jsonify(error=str(e)), 500


@app.route('/removeAccount', methods=['GET'])
@login_required
def removeAccount():
    try:
        user_id = session.get('session')
        identification_token = request.args.get('token')
        if db.check_token(identification_token):
            token_user_id = db.get_userid_by_token(identification_token)
            if token_user_id == user_id:
                user = db.get_user_by_id(user_id)
                if user is None:
                    flash("You are not logged in", "danger")
                    return redirect(url_for('login'))
                else:
                    # delete the user directory
                    user_workdir = user.workdir
                    user_workdir_path = os.path.join(app.config['WORK_DIRS'], user_workdir)
                    shutil.rmtree(user_workdir_path)
                    # delete the user from the database
                    db.delete_user(user_id)
                    # delete the user session
                    session.pop('session', None)

                    # Update statistics
                    last_statistics = db.get_last_statistics()
                    if db.last_statistics_is_today(last_statistics):
                        last_statistics.users_count -= 1
                        db.update_statistics(last_statistics)
                    else:
                        last_statistics.users_count -= 1
                        db.insert_statistics(last_statistics)

                    # Log account deletion
                    logger.info(f"User {user.id} deleted their account")

                    flash("Account deleted successfully", "success")
                    return redirect(url_for('login'))
            else:
                flash("Invalid token", "danger")
                return redirect(url_for('home'))
        else:
            flash("Invalid token", "danger")
            return redirect(url_for('home'))
    except Exception as e:
        flash("Error in removeAccount route", "danger")
        logger.error(f"Error in removeAccount route: {e}")
        return jsonify(error=str(e)), 500


@app.route('/markdownify_app/download', methods=['GET'])
def app_download():
    try:
        # check if path exists
        app_path = app.config['APP_PATH']
        if not os.path.exists(app_path):
            logger.error("App file not found")
            return "File not found", 404
        else:
            # update statistics
            last_statistics = db.get_last_statistics()
            if db.last_statistics_is_today(last_statistics):
                last_statistics.app_downloads += 1
                db.update_statistics(last_statistics)
            else:
                last_statistics.app_downloads += 1
                db.insert_statistics(last_statistics)

            # Log app download
            logger.info("App download request")

            return send_file('static/app/markdownify_app.zip', as_attachment=True), 200
    except Exception as e:
        logger.error(f"Error in app_download route: {e}")
        return jsonify(error=str(e)), 500

@app.route('/lostPassword', methods=['GET', 'POST'])
def lostPassword():
    try:
        # if logged in, redirect to home
        user_id = session.get('session')
        if user_id is not None:
            return redirect(url_for('home'))
        else:
            if request.method == 'POST':
                email = request.form.get('email')
                user = db.get_user_by_email(email)
                if user is None:
                    flash("Email not found", "danger")
                    return redirect(url_for('lostPassword'))
                else:
                    token = secrets.token_urlsafe(16)
                    db.update_user_mail_token(user.id, token)
                    # send email
                    try:
                        password_reset_email_sender.send_password_reset_email(email, token)
                        flash("Email sent successfully", "success")

                        # Log password reset email sent
                        logger.info(f"Password reset email sent to {email}")

                        return redirect(url_for('resetPasswordSend'))
                    except Exception as e:
                        flash("Error while sending email", "danger")

                        # Log error sending password reset email
                        logger.error(f"Error sending password reset email: {e}")

                        return redirect(url_for('lostPassword'))
        return render_template('lostPassword.html')
    except Exception as e:
        logger.error(f"Error in lostPassword route: {e}")
        return jsonify(error=str(e)), 500

@app.route('/resetPassword', methods=['GET', 'POST'])
def resetPassword():
    try:
        # check if url token is valid
        token = request.args.get('token')
        if request.method == 'POST':
            password = request.form['new-password']
            password2 = request.form['confirm-password']
            form_token = request.form['token']
            user = db.get_user_by_mail_token(form_token)
            # if passwords match
            if user is not None and password == password2:
                try:
                    db.update_user_password(password, user.id)
                    db.remove_email_token(user)
                    flash("Password updated", "success")

                    # Log password update
                    logger.debug(f"Password updated for user {user.id}")
                    logger.log("SECURITY-Lv2", f"Password updated for user {user.id}")

                    return redirect(url_for('login'))
                except Exception as e:
                    flash("Error updating password", "danger")

                    # Log error updating password
                    logger.error(f"Error updating password for user {user.id}: {e}")

                    return redirect(url_for('lostPassword'))
            else:
                flash("Invalid token", "danger")

                # Log invalid token
                logger.debug(f"Invalid token")
                logger.log("SECURITY-Lv2", f"Invalid token {form_token} for user {user.id}")

                return redirect(url_for('lostPassword'))
        return render_template('resetPassword.html', logged_in=False, token=token)
    except Exception as e:
        logger.error(f"Error in resetPassword route: {e}")
        return jsonify(error=str(e)), 500

@app.route('/resetPasswordSend', methods=['GET', 'POST'])
def resetPasswordSend():
    try:
        if request.method == 'POST':
            # Handle POST request data if needed
            pass

        # Log resetPasswordSend route access
        logger.info("Accessed resetPasswordSend route")

        return render_template('resetPasswordSend.html')
    except Exception as e:
        logger.error(f"Error in resetPasswordSend route: {e}")
        return jsonify(error=str(e)), 500

@app.route('/admin', methods=['GET', 'POST'])
@lexit_required
def admin():
    try:
        user_id = session.get('session')
        # if session Get the user from the database
        user = db.get_user_by_id(user_id)
        if user is None:
            flash("You are not logged in", "danger")
            return redirect(url_for('login'))
        else:
            users = db.get_all_users()
            statistics = db.get_all_statistics()
            monthly_statistics = db.get_monthly_statistics()
            weakly_statistics = db.get_weekly_statistics()
            statistics_json = [statistic.json() for statistic in statistics]
            weakly_statistics_json = [statistic.json() for statistic in weakly_statistics]
            monthly_statistics_json = [statistic.json() for statistic in monthly_statistics]
            verification_token = db.renew_token(user.id)

            # if post request
            if request.method == 'POST':
                # Handle POST request data if needed
                pass

            # Log admin route access
            logger.info("Accessed admin route")

            return render_template('admin.html', user=user, name=user.name,
                                   profile_image_path=user.profile_image, logged_in=True, role=user.role, users=users,
                                   statistics=statistics_json, monthly_statistics=monthly_statistics_json,
                                   weakly_statistics=weakly_statistics_json,verification_token=verification_token)
    except Exception as e:
        logger.error(f"Error in admin route: {e}")
        return jsonify(error=str(e)), 500

@app.route('/app_upload', methods=['POST'])
def app_upload():
    try:
        app_file = request.files.get('app')
        verification_token = request.form.get('token')
        if verification_token is not None:
            token_user_id = db.get_userid_by_token(verification_token)
            if token_user_id is None:
                return "Invalid token", 403
            user = db.get_user_by_id(token_user_id)
            if user is None:
                return "Invalid token", 403
            # check if user is admin
            if token_user_id is None:
                return "Invalid token", 403
            if user.role != "admin":
                return "You are not an admin", 403
            db.remove_token(user.id)

        # Handle app file upload and renaming if needed
        file_path = app.config['APP_PATH']
        if os.path.exists(file_path):
            os.remove(file_path)
        app_file.save(file_path)

        return "OK"
    except Exception as e:
        logger.error(f"Error in app_upload route: {e}")
        return jsonify(error=str(e)), 500

@app.route('/readme_upload',methods=['POST'])
def readme_upload():
    try:
        readme_file = request.files.get('readme')
        verification_token = request.form.get('token')
        if verification_token is not None:
            token_user_id = db.get_userid_by_token(verification_token)
            if token_user_id is None:
                return "Invalid token", 403
            user = db.get_user_by_id(token_user_id)
            if user is None:
                return "Invalid token", 403
            # check if user is admin
            if token_user_id is None:
                return "Invalid token", 403
            if user.role != "admin":
                return "You are not an admin", 403
            db.remove_token(user.id)

        # Handle readme file upload and renaming if needed
        file_path = app.config['README_FILE']
        if os.path.exists(file_path):
            os.remove(file_path)
        readme_file.save(file_path)

        # Log readme file upload
        logger.info("Uploaded readme file")

        return "OK"
    except Exception as e:
        logger.error(f"Error in readme_upload route: {e}")
        return jsonify(error=str(e)), 500



if __name__ == '__main__':
    try:
        app.run(debug=False)
    except Exception as e:
        print(e)
